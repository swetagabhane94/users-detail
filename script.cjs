fetch("https://fakestoreapi.com/users/")
.then((response) => {
    if (!response.ok) {
      dispayFailedFetchDocuement();
    } else {
      return response.json();
    }
  })
.then((data) => {
    console.log("Data fetch");
    loaderBlocker();
    if (data === null) {
      noProductToShow();
    } else if (!Array.isArray(data)) {
      singleData(data);
    } else if (data.length === 0) {
      noProductToShow();
    } else {
      myStore(data);
    }
  })
.catch((err) => {
    console.error(`error occured on fetching file ${err}`);
    loaderBlocker();
    dispayFailedFetchDocuement();
  });

function myStore(detail) {
  detail.forEach((data) => {
    singleData(data);
  });
}

function singleData(data) {
  const unorderList = document.querySelector("ul");
  const list = document.createElement("li");
  const fullName = document.createElement("p");
  fullName.className = "full-name";
  const userName = document.createElement("p");
  userName.className = "detail";
  const email = document.createElement("p");
  email.className = "detail";
  const phoneNo = document.createElement("p");
  phoneNo.className = "detail";
  const address = document.createElement("p");
  address.className = "detail";

  fullName.textContent = `${data.name.firstname.toUpperCase()} ${data.name.lastname.toUpperCase()}`;
  userName.textContent = `Username: ${data.username}`;
  email.textContent = `Email: ${data.email}`;
  phoneNo.textContent = `Phone: ${data.phone}`;
  address.textContent = `Address: ${data.address.number}, ${data.address.street}, ${data.address.city}, zipcode- ${data.address.zipcode}`;

  list.append(fullName, userName, email, phoneNo, address);
  unorderList.append(list);
}

function dispayFailedFetchDocuement() {
  const errorDisplay = document.querySelector(".failed-fetch");
  errorDisplay.style.display = "block";
}

function loaderBlocker() {
  const loader = document.querySelector(".center");
  loader.remove();
}

function noProductToShow() {
  const failFetch = document.querySelector(".empty-list");
  failFetch.style.display = "block";
}
